# frozen_string_literal: true

require 'spec_helper'

describe 'query with forbidden labels' do
  include_context 'with integration context'

  before do
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues?labels=foo&not[labels]=bar,baz",
      query: { per_page: 100 },
      headers: { 'PRIVATE-TOKEN' => token }) do
      [issue]
    end
  end

  it 'comments on the issue with ~foo, and not ~bar nor ~baz' do
    rule = <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Nudge on issues with closed milestone
              conditions:
                labels:
                  - foo
                forbidden_labels:
                  - bar
                  - baz
              actions:
                comment: |
                  This issue has ~foo, and not ~bar nor ~baz!
    YAML

    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue[:iid]}/notes",
      body: { body: 'This issue has ~foo, and not ~bar nor ~baz!' },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end
end
